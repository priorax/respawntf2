#!/usr/bin/python

import os
import argparse

def update():
    os.system("/tf2/steamcmd.sh +runscript tf2_ds.txt")

def start(hostname,cfgSet):
    os.system("/tf2/tf2/srcds_run -game tf +sv_lan 1 " + mapDetails + " hostname = \"" + hostname + "\" " + svType)

parser = argparse.ArgumentParser()
parser.add_argument("-n","--hostname",help="Set's servers hostnme")
parser.add_argument("-t","--svType",help="Specifies the type of server to be launched")
parser.add_argument("-m","--startMap",help"Specifies the starting map")
parser.add_argument("-p","--players",help="Specifies number of max players")

args = parser.parse_args()

if not args.svType:
    svType = "pub"
else:
    if str.lower(args.svType) == "comp":
        svType = "comp"
    else:
        svType = "pub"

if args.hostname:
    hostname = args.hostname
else:
    hostname = "Respawn TF2 " + svType + " server"

cfgSet = ""
if svType == "pub"
 cfgSet = "+exec pub"
elif svType = "comp"
 cfgSet = "+exec comp"


if args.startMap:
    cfgSet += "  +map " + args.startMap

if args.players:
    cfgSet += " +maxplayers " + args.players 

update()
start(hostname,cfgSet)
